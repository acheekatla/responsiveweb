const Fourcolumn =()=>{
  return(
    <>
   <section className="one-fourth" id="html">
    <td><i className="fa fa-html5"></i></td>
    <h3>HTML</h3>
  </section>
 <section className="one-fourth" id="css">
 <td><i className="fa fa-css3"></i></td>
   <h3>CSS</h3>
 </section>
 <section className="one-fourth" id="seo">
   <td><i className="fa fa-search"></i></td>
   <h3>SEO</h3>
 </section>
 <section className="one-fourth" id="social">
   <td><i className="fa fa-users"></i></td>
   <h3>Social</h3>
 </section></>
  );
}
export default Fourcolumn