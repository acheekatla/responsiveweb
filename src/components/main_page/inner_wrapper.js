const Wrapper = () => {
  return (
    <>
      <section className="inner-wrapper">
        <article id="tablet">
          <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_ipad_vr0pfu.png" />
        </article>
        <aside id="tablet2">
          <h2>Mobile.Tablet.Desktop.</h2>
          <p>
            A clean HTML layout and CSS stylesheet making for a great responsive
            framework to design around that includes a responsive drop down
            navigation menu, image slider, contact form and ‘scroll to the top’
            jQuery plugin.jQuery plugin.that includes a responsive drop down
            navigation menu, image slider, contact form and ‘scroll to the top’
            jQuery plugin.
          </p>
        </aside>
      </section>
      <section className="inner-wrapper-2">
        <article id="mobile">
          <h2>Accross Each Device</h2>
          <p>
            A clean HTML layout and CSS stylesheet making for a great responsive
            framework to design around that includes a responsive drop down
            navigation menu, image slider, contact form and ‘scroll to the top’
            jQuery plugin.that includes a responsive drop down navigation menu,
            image slider, contact form and ‘scroll to the top’ jQuery plugin.
          </p>
        </article>
        <aside className="hand-mobile">
          <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_mobile_stt5sh.png" />
        </aside>
      </section>
      <section className="inner-wrapper">
        <article>
          <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571256/desktop_fsksnx.png" />
        </article>
        <aside id="desktop">
          <h2>Desktop</h2>
          <p>
            Also included with the Template is the Template Customization Guide
            with five special video lessons showing you how to get professional
            website pictures & how to edit them to fit the template, how to make
            a custom website logo, how to upload your HTML website template to
            the internet and an HTML website building tools video!
          </p>
        </aside>
      </section>
      <section class="inner-wrapper-3"></section>
    </>
  );
};
export default Wrapper;
