import "./index.css";
import Header from "./common/header";
import Banner from "./main_page/banner";
import Fourcolumn from "./main_page/fourcol_section";
import Wrapper from "./main_page/inner_wrapper";
import Onethird from "./main_page/onethird";
import Smelly from "./main_page/smellysection";
import Footer from "./common/footer.js/footer1";
import Footers from "./common/footer.js/footer2";

const Source = () => {
  return (
    <body>
      <Header />
      {/* <!---end of header---> */}
      <Banner />
      <Fourcolumn />
      {/* // <!--end of four col section--> */}
      <Wrapper />
      {/* <!--end of two half section--> */}

      <Onethird />
      {/* <!--end of one-third section--> */}
      <Smelly />
      {/* <!--end of smelly section--> */}
      <Footer />

      {/* <!--end of footer--> */}
      <Footers />
      {/* <!--end second footer--> */}
    </body>
  );
};

export default Source;
